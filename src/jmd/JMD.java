/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmd;

import java.util.logging.Level;
import java.util.logging.Logger;
import jmd.owlcore.OWLTool;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;

/**
 *
 * @author Евгений
 */
public class JMD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            
            OWLTool owl_tool = new OWLTool();
            owl_tool.readOntologyFromLocalFile("D:/test_rdf_xml.owl");
            
            
        } catch (OWLOntologyCreationException ex) {
            
            Logger.getLogger(JMD.class.getName()).log(Level.SEVERE, null, ex);
            
        }

    }
    
}