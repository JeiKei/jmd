/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jmd.owlcore;

import java.io.File;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;

/**
 *
 * @author Евгений
 */
public class OWLTool {
    
    private OWLOntology ontology;
    private OWLDataFactory dataFactory;
        
    
    /**
     * Read ontology from file
     * 
     * @param filename
     * @return
     * @throws OWLOntologyCreationException 
     */
    public void readOntologyFromLocalFile(String filename) throws OWLOntologyCreationException {
        
        OWLOntologyManager owlManager = OWLManager.createOWLOntologyManager();
        File ontologyOwlFile = new File(filename);
        ontology = owlManager.loadOntologyFromOntologyDocument(ontologyOwlFile);
        dataFactory = owlManager.getOWLDataFactory();
        OWLClass thing = dataFactory.getOWLThing();
        
    }
    
}